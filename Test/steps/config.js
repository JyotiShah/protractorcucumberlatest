"use strict";
exports.__esModule = true;
exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    //seleniumServerJar: '../../node_modules/webdriver-manager/selenium/selenium-server-standalone-3.141.59.jar',
    specs: ["../features/*.feature"],
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    baseUrl: "https://angular.io/",
    cucumberOpts: {
        compiler: "ts:ts-node/register",
        strict: true,
        format: ['pretty'],
        require: ['../steps/*.js', '../hooks/*.js'],
        tags: '@smoke2'
    }
};

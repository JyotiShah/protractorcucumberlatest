import { browser, element, by, protractor, $$, $, ExpectedConditions } from 'protractor';

describe("Test Spec", () => {

    it("navigate to application", async () => {
       await browser.manage().window().maximize();
       await browser.get("https://angular.io/");
       await browser.getTitle().then(function(text){
         expect(text).toEqual('Angular');
       });
       await browser.sleep(2000);
    });

     it("click on features", async() => {
       await element(by.css("a[title='Features']")).click();
       await browser.sleep(4000);
     });

     it("Search Jasmine", async() => {
        await element(by.css(".search-container input")).sendKeys("Jasmine");
        await browser.wait(ExpectedConditions.visibilityOf($('.ng-star-inserted .search-results')),5000);
        await $$('.search-result-item').then(function(items) {
            browser.sleep(2000);
            items[0].click();
          });
    });
});





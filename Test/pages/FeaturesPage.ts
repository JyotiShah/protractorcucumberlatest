

import { BasePage, IdentificationType } from "./BasePage";
import { browser } from "protractor";

const Locators = {

    Duration:{
        type: IdentificationType[IdentificationType.Xpath],
        value: "//Duration"
    },
    courseHeading:{
        type:IdentificationType[IdentificationType.Xpath],
        value:"//h2"
    },
    FeaturesAndBenefitsHeading :{
        type: IdentificationType[IdentificationType.Css],
        value:"header.marketing-banner #features--benefits"
    }

}


export class FeaturesPage extends BasePage{


    featuresAndBenefitsHeading = this.ElementLocator(Locators.FeaturesAndBenefitsHeading);

    async getHeadingName(){
        browser.sleep(2000);
        await this.featuresAndBenefitsHeading.getText().then((text) => {
          console.log("-----------------",text);
            return text;
     });
    }

   
}